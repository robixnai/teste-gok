//
//  StoreDelegates.swift
//  gok-challenge
//
//  Created by Robson Moreira on 09/11/20.
//

import UIKit

protocol ProductsStore: GenericStore {
    func getProucts(completion: @escaping completion<Products?>)
}

protocol ImageStore: GenericStore {
    func getImage(url: URL, completion: @escaping completion<UIImage?>)
}
