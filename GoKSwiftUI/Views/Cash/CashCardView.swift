//
//  CashCardView.swift
//  gok-challenge
//
//  Created by Robson Moreira on 12/11/20.
//

import SwiftUI

struct CashCardView: View {
    
    let image: UIImage
    
    var body: some View {
        VStack{
            Image(uiImage: image)
                .resizable()
                .frame(width: UIScreen.main.bounds.width - 50, height: 100)
        }
        .cornerRadius(10)
    }
}

struct CashCardView_Previews: PreviewProvider {
    static var previews: some View {
        CashCardView(image: #imageLiteral(resourceName: "cash"))
    }
}
