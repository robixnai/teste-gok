//
//  ProductsView.swift
//  gok-challenge
//
//  Created by Robson Moreira on 09/11/20.
//

import SwiftUI

struct ProductsView: View {
    
    @State private var showingAlert = false
    @ObservedObject var viewModel = ProductsViewModel()
    
    var body: some View {
        NavigationView {
            VStack {
                SpotlightCollectionView(viewModel: self.viewModel.spotlightViewModel).padding(.bottom, 20)
                CashView(viewModel: self.viewModel.cashViewModel).padding(.bottom, 40)
                ProductCollectionView(viewModel: self.viewModel.productViewModel)
            }
            .alert(isPresented: $showingAlert) {
                Alert(title: Text("Erro"), message: Text("Erro ao buscar os Produtos"), dismissButton: .default(Text("OK")))
            }
            .navigationBarItems(leading: VStack {
                HStack {
                    Image("digio")
                        .resizable()
                        .frame(width: 40, height: 40, alignment: .center)
                    Text("Olá, Maria")
                        .font(.title)
                        .foregroundColor(Color(red: 87 / 255, green: 87 / 255, blue: 87 / 255))
                }.padding(.top, 48)
            })
        }.onAppear {
            self.viewModel.getProducts()
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        ProductsView()
    }
}
