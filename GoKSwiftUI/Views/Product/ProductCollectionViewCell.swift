//
//  ProductCollectionViewCell.swift
//  gok-challenge
//
//  Created by Robson Moreira on 12/11/20.
//

import SwiftUI

struct ProductCollectionViewCell: View {
    
    let image: UIImage
    
    var body: some View {
        VStack(alignment: .leading) {
            Image(uiImage: image)
                .resizable()
                .frame(width: 50, height: 50, alignment: .center)
        }
    }
}

struct ProductCollectionViewCell_Previews: PreviewProvider {
    static var previews: some View {
        ProductCollectionViewCell(image: #imageLiteral(resourceName: "product"))
    }
}
