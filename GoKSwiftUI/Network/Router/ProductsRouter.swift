//
//  ProductsRouter.swift
//  gok-challenge
//
//  Created by Robson Moreira on 09/11/20.
//

import Foundation

enum ProductsRouter {
    
    case products
    
    var path: String {
        switch self {
        case .products:
            return API.products
        }
    }
    
    func asURLRequest() throws -> URL? {
        guard let url = URL(string: API.baseURL) else { return nil }
        let urlRequest = URLRequest(url: url.appendingPathComponent(path))
        switch self {
        case .products:
            return urlRequest.url
        }
    }
    
}
