//
//  SpotlightCollectionViewCell.swift
//  gok-challenge
//
//  Created by Robson Moreira on 11/11/20.
//

import SwiftUI

struct SpotlightCollectionViewCell: View {
    
    let image: UIImage
    
    var body: some View {
        VStack{
            Image(uiImage: self.image)
                .resizable()
                .frame(width: UIScreen.main.bounds.width - 50, height: 156)
        }
        .cornerRadius(10)
        .shadow(color: .gray, radius: 4, x: 0.0, y: 10.0)
    }
}

struct SpotlightCollectionViewCell_Previews: PreviewProvider {
    static var previews: some View {
        SpotlightCollectionViewCell(image: #imageLiteral(resourceName: "spotlight"))
    }
}
