//
//  ImageAPIStore.swift
//  GoKSwiftUI
//
//  Created by Robson Moreira on 13/11/20.
//

import Alamofire
import AlamofireImage

class ImageAPIStore: GenericStore, ImageStore {
    
    func getImage(url: URL, completion: @escaping (UIImage?, Error?) -> Void) {
        AF.request(url, method: .get).responseImage { response in
            if case .success(let image) = response.result {
                completion(image, nil)
            } else {
                completion(nil, response.error)
            }
        }
    }
    
}
