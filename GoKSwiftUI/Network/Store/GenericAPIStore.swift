//
//  GenericAPIStore.swift
//  gok-challenge
//
//  Created by Robson Moreira on 09/11/20.
//

import Foundation

protocol GenericStore {
    typealias completion<T> = (_ result: T, _ failure: Error?) -> Void
}

class GenericAPIStore {
    
    var error = NSError(domain: "", code: 901, userInfo: [NSLocalizedDescriptionKey: "Error getting information"])
    
}
