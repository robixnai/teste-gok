//
//  SpotlightViewModel.swift
//  gok-challenge
//
//  Created by Robson Moreira on 12/11/20.
//

import SwiftUI

class SpotlightViewModel: ObservableObject {
    
    @Published var images = [UIImage]()
    
    var dataProvider: ImageDataProviderManeger
    
    var spotlights: [Spotlight]? {
        didSet {
            self.getImages()
        }
    }
    
    init(dataProvider: ImageDataProviderManeger = ImageDataProviderManeger()) {
        self.dataProvider = dataProvider
        self.dataProvider.delegate = self
    }
    
    fileprivate func getImages() {
        guard let spotlights = self.spotlights else { return }
        
        for spotlight in spotlights {
            if let url = URL(string: spotlight.bannerURL) {
                self.dataProvider.getImage(url: url)
            }
        }
    }
    
}

extension SpotlightViewModel: ImageDataProviderProtocol {
    
    func success(object: Any) {
        if let image = object as? UIImage {
            self.images.append(image)
        }
    }
    
    func errorData(_ provider: GenericDataProvider?, error: NSError) {
        self.images.append(UIImage(imageLiteralResourceName: "notImage"))
    }
    
}
