//
//  Cash.swift
//  gok-challenge
//
//  Created by Robson Moreira on 09/11/20.
//

import Foundation

struct Cash: Codable {
    
    var title: String
    var bannerURL: String
    var description: String
    
}
