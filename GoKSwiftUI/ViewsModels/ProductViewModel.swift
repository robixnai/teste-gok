//
//  ProductViewModel.swift
//  gok-challenge
//
//  Created by Robson Moreira on 12/11/20.
//

import SwiftUI

class ProductViewModel: ObservableObject {
    
    @Published var images = [UIImage]()
    
    var dataProvider: ImageDataProviderManeger
    
    var products: [Product]? {
        didSet {
            self.getImages()
        }
    }
    
    init(dataProvider: ImageDataProviderManeger = ImageDataProviderManeger()) {
        self.dataProvider = dataProvider
        self.dataProvider.delegate = self
    }
    
    fileprivate func getImages() {
        guard let products = self.products else { return }
        
        for product in products {
            if let url = URL(string: product.imageURL) {
                self.dataProvider.getImage(url: url)
            }
        }
    }
    
}

extension ProductViewModel: ImageDataProviderProtocol {
    
    func success(object: Any) {
        if let image = object as? UIImage {
            self.images.append(image)
        }
    }
    
    func errorData(_ provider: GenericDataProvider?, error: NSError) {
        self.images.append(UIImage(imageLiteralResourceName: "notImage"))
    }
    
}
