//
//  GoKSwiftUIApp.swift
//  GoKSwiftUI
//
//  Created by Robson Moreira on 12/11/20.
//

import SwiftUI

@main
struct GoKSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ProductsView()
        }
    }
}
