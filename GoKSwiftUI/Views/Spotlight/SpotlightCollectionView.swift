//
//  SpotlightCollectionView.swift
//  gok-challenge
//
//  Created by Robson Moreira on 11/11/20.
//

import SwiftUI

struct SpotlightCollectionView: View {
    
    @ObservedObject var viewModel: SpotlightViewModel
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack(spacing: 20) {
                ForEach(self.viewModel.images, id: \.self) { image in
                    SpotlightCollectionViewCell(image: image)
                }
            }
            .padding(20)
        }
    }
}

struct SpotlightCollectionView_Previews: PreviewProvider {
    static var previews: some View {
        SpotlightCollectionView(viewModel: SpotlightViewModel())
    }
}
