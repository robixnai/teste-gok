//
//  Spotlight.swift
//  gok-challenge
//
//  Created by Robson Moreira on 09/11/20.
//

import Foundation

struct Spotlight: Codable {
    
    var name: String
    var bannerURL: String
    var description: String
    
}
