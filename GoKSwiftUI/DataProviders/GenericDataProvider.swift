//
//  GenericDataProvider.swift
//  gok-challenge
//
//  Created by Robson Moreira on 09/11/20.
//

import Foundation

protocol GenericDataProvider: class {
    
    func success(object: Any)
    func errorData(_ provider: GenericDataProvider?, error: NSError)
    
}

class DataProviderManager<T, S> {
    
    var delegate: T?
    
}
