//
//  ProductsDataProvider.swift
//  gok-challenge
//
//  Created by Robson Moreira on 09/11/20.
//

import Foundation

protocol ProductsDataProviderProtocol: GenericDataProvider {}

class ProductsDataProviderManeger: DataProviderManager <ProductsDataProviderProtocol, Products> {
    
    func getProducts() {
        ProductsAPIStore().getProucts { (products, error) in
            if error == nil {
                if let products = products {
                    self.delegate?.success(object: products)
                }
            } else {
                self.delegate?.errorData(self.delegate, error: error! as NSError)
            }
        }
    }
    
}
