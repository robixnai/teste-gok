//
//  ImageDataProvider.swift
//  GoKSwiftUI
//
//  Created by Robson Moreira on 13/11/20.
//

import UIKit

protocol ImageDataProviderProtocol: GenericDataProvider {}

class ImageDataProviderManeger: DataProviderManager <ImageDataProviderProtocol, UIImage> {
    
    func getImage(url: URL) {
        ImageAPIStore().getImage(url: url) { (image, error) in
            if error == nil {
                if let image = image {
                    self.delegate?.success(object: image)
                }
            } else {
                self.delegate?.errorData(self.delegate, error: error! as NSError)
            }
        }
    }
    
}
