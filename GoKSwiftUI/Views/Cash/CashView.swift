//
//  CashView.swift
//  gok-challenge
//
//  Created by Robson Moreira on 12/11/20.
//

import SwiftUI

struct CashView: View {
    
    private let primaryColor = Color(red: 32 / 255, green: 39 / 255, blue: 69 / 255)
    private let secondaryColor = Color(red: 144 / 255, green: 144 / 255, blue: 144 / 255)
    
    @ObservedObject var viewModel: CashViewModel
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                ForEach(0..<self.viewModel.title.count, id: \.self) { index in
                    Text(self.viewModel.title[index])
                        .font(.body)
                        .foregroundColor(index.isMultiple(of: 2) ? self.primaryColor : self.secondaryColor)
                }
            }
            CashCardView(image: self.viewModel.image)
        }
        .cornerRadius(10)
    }
}

struct CashView_Previews: PreviewProvider {
    static var previews: some View {
        CashView(viewModel: CashViewModel())
    }
}
