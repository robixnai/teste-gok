//
//  ProductsAPIStore.swift
//  gok-challenge
//
//  Created by Robson Moreira on 09/11/20.
//

import Alamofire

class ProductsAPIStore: GenericAPIStore, ProductsStore {
    
    func getProucts(completion: @escaping (Products?, Error?) -> Void) {
        do {
            let urlRequest = try ProductsRouter.products.asURLRequest()
            if let url = urlRequest {
                AF.request(url, method: .get, encoding: URLEncoding.queryString).responseJSON { response in
                    switch response.result {
                    case .success:
                        guard let json = response.data else {
                            completion(nil, self.error)
                            return
                        }
                        
                        do {
                            let result = try JSONDecoder().decode(Products.self, from: json)
                            completion(result, nil)
                        } catch {
                            completion(nil, error)
                        }
                    case .failure(let error):
                        completion(nil, error)
                    }
                    
                }
            }
        } catch let error {
            completion(nil, error)
        }
    }
    
}
