//
//  ProductCollectionView.swift
//  gok-challenge
//
//  Created by Robson Moreira on 12/11/20.
//

import SwiftUI

struct ProductCollectionView: View {
    
    @ObservedObject var viewModel: ProductViewModel
    
    var body: some View {
        VStack(alignment: .leading) {
            VStack {
                Text("Produtos")
                    .font(.body)
                    .foregroundColor(Color(red: 32 / 255, green: 39 / 255, blue: 69 / 255))
            }.padding(.leading, 26)
            
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 20) {
                    ForEach(self.viewModel.images, id: \.self) { image in
                        ProductCollectionViewCell(image: image)
                            .frame(width: 100, height: 100)
                            .background(Color.white)
                            .cornerRadius(10)
                            .shadow(color: .gray, radius: 7.2, x: 0.0, y: 10.0)
                    }
                }
                .padding(.bottom, 40)
                .padding(.leading, 40)
                .padding(.trailing, 40)
            }
        }
    }
}

struct ProductCollectionView_Previews: PreviewProvider {
    static var previews: some View {
        ProductCollectionView(viewModel: ProductViewModel())
    }
}
