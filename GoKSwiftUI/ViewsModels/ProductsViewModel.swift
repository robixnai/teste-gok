//
//  ProductsViewModel.swift
//  gok-challenge
//
//  Created by Robson Moreira on 09/11/20.
//

import SwiftUI

class ProductsViewModel: ObservableObject {
    
    @Published var spotlightViewModel: SpotlightViewModel
    @Published var cashViewModel: CashViewModel
    @Published var productViewModel: ProductViewModel
    
    var dataProvider: ProductsDataProviderManeger
    
    init(dataProvider: ProductsDataProviderManeger = ProductsDataProviderManeger()) {
        self.spotlightViewModel = SpotlightViewModel()
        self.cashViewModel = CashViewModel()
        self.productViewModel = ProductViewModel()
        
        self.dataProvider = dataProvider
        self.dataProvider.delegate = self
    }
    
    func getProducts() {
        self.dataProvider.getProducts()
    }
    
}

extension ProductsViewModel: ProductsDataProviderProtocol {
    
    func success(object: Any) {
        if let products = object as? Products {
            self.spotlightViewModel.spotlights = products.spotlight
            self.cashViewModel.cash = products.cash
            self.productViewModel.products = products.products
        }
    }
    
    func errorData(_ provider: GenericDataProvider?, error: NSError) {
        print("\(error.description)")
    }
    
}
