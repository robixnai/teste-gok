//
//  Product.swift
//  gok-challenge
//
//  Created by Robson Moreira on 09/11/20.
//

import Foundation

struct Product: Codable {
    
    var name: String
    var imageURL: String
    var description: String
    
}
