//
//  API.swift
//  gok-challenge
//
//  Created by Robson Moreira on 09/11/20.
//

import Foundation

struct API {
    
    static let baseURL = "https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/sandbox"
    
    static let products = "products"
    
}
