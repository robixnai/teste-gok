//
//  Products.swift
//  gok-challenge
//
//  Created by Robson Moreira on 09/11/20.
//

import Foundation

struct Products: Codable {
    
    var spotlight: [Spotlight]
    var products: [Product]
    var cash: Cash
    
}
