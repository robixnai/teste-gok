//
//  CashViewModel.swift
//  gok-challenge
//
//  Created by Robson Moreira on 12/11/20.
//

import SwiftUI

class CashViewModel: ObservableObject {
    
    @Published var title = [String]()
    @Published var image = UIImage()
    
    var dataProvider: ImageDataProviderManeger
    
    var cash: Cash? {
        didSet {
            self.setTitle()
            self.getImages()
        }
    }
    
    init(dataProvider: ImageDataProviderManeger = ImageDataProviderManeger()) {
        self.dataProvider = dataProvider
        self.dataProvider.delegate = self
    }
    
    fileprivate func setTitle() {
        guard let cash = self.cash else { return }
        
        self.title = cash.title.split(separator: " ").map(String.init)
    }
    
    fileprivate func getImages() {
        guard let cash = self.cash else { return }
        
        if let url = URL(string: cash.bannerURL) {
            self.dataProvider.getImage(url: url)
        }
    }
    
}

extension CashViewModel: ImageDataProviderProtocol {
    
    func success(object: Any) {
        if let image = object as? UIImage {
            self.image = image
        }
    }
    
    func errorData(_ provider: GenericDataProvider?, error: NSError) {
        self.image = UIImage(imageLiteralResourceName: "notImage")
    }
    
}
